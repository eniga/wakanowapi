﻿using System;
namespace WakanowApi.Models
{
    public class CustomerInformationRequestPayload
    {
        public string PaymentItemCode { get; set; }
        public string MerchantReference { get; set; }
        public string CustReference { get; set; }
    }

    public class HashRequestPayload
    {
        public string PaymentItemCode { get; set; }
        public string MerchantReference { get; set; }
        public string CustReference { get; set; }
    }

    public class HashRequestPayload2
    {
        public string PaymentItemCode { get; set; }
        public string MerchantReference { get; set; }
        public string CustReference { get; set; }
    }

    public class PaymentNotificationRequestPayload
    {
        public Payments Payments { get; set; }
    }
}
