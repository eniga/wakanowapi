﻿using System;
namespace WakanowApi.Models
{
    public class HashRequest
    {
        public string PaymentItemCode { get; set; }
        public string MerchantReference { get; set; }
        public string CustReference { get; set; }
        public string HASHKEY { get; set; }
    }

    public class CustomerInformationRequest
    {
        public string PaymentItemCode { get; set; }
        public string Service { get; set; } = "bill-payment-service";
        public string MerchantReference { get; set; }
        public string CustReference { get; set; }
        public string Hash { get; set; }
    }

    public class PaymentNotificationRequest
    {
        public Payments Payments { get; set; }
        public string Hash { get; set; }
    }

    public class Payments
    {
        public Payment Payment { get; set; }
    }

    public class Payment
    {
        public string PaymentLogId { get; set; }
        public string CustReference { get; set; }
        public string AlternateCustReference { get; set; }
        public string Amount { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentReference { get; set; }
        public string ChannelName { get; set; }
        public string PaymentDate { get; set; } = DateTime.Now.ToString();
        public string BankName { get; set; }
        public string CustomerName { get; set; }
        public string PaymentCurrency { get; set; } = "566";
        public string PaymentCurrencyCode { get; set; } = "NGN";
        public PaymentItems2 PaymentItems { get; set; }
        public string PaymentStatus { get; set; }
    }

    public class PaymentItems2
    {
        public PaymentItem2 PaymentItem { get; set; }
    }

    public class PaymentItem2
    {
        public string ItemCode { get; set; } = "itemcode";
        public string ItemAmount { get; set; }
    }
}
