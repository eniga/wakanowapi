﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace WakanowApi.Models
{
    [XmlRoot(ElementName = "CustomerInformationResponse")]
    public class CustomerInformationResponse
    {
        [XmlElement(ElementName = "MerchantReference")]
        public string MerchantReference { get; set; }
        [XmlElement(ElementName = "Customers")]
        public Customers Customers { get; set; }
        [XmlElement(ElementName = "PaymentCurrency")]
        public string PaymentCurrency { get; set; }
        [XmlElement(ElementName = "PaymentCurrencyCode")]
        public string PaymentCurrencyCode { get; set; }
        [XmlElement(ElementName = "Hash")]
        public string Hash { get; set; }
        [XmlAttribute(AttributeName = "Version")]
        public string Version { get; set; }
    }

    [XmlRoot(ElementName = "Customers")]
    public class Customers
    {
        [XmlElement(ElementName = "Customer")]
        public Customer Customer { get; set; }
    }

	[XmlRoot(ElementName = "Customer")]
	public class Customer
	{
		[XmlElement(ElementName = "Status")]
		public string Status { get; set; }
		[XmlElement(ElementName = "StatusMessage")]
		public string StatusMessage { get; set; }
		[XmlElement(ElementName = "CustReference")]
		public string CustReference { get; set; }
		[XmlElement(ElementName = "CustomerReferenceAlternate")]
		public string CustomerReferenceAlternate { get; set; }
		[XmlElement(ElementName = "CustomerReferenceDescription")]
		public string CustomerReferenceDescription { get; set; }
		[XmlElement(ElementName = "FirstName")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "LastName")]
		public string LastName { get; set; }
		[XmlElement(ElementName = "Email")]
		public string Email { get; set; }
		[XmlElement(ElementName = "Phone")]
		public string Phone { get; set; }
		[XmlElement(ElementName = "ThirdPartyCode")]
		public string ThirdPartyCode { get; set; }
		[XmlElement(ElementName = "Amount")]
		public string Amount { get; set; }
		[XmlElement(ElementName = "PaymentItems")]
		public PaymentItems PaymentItems { get; set; }
	}

	[XmlRoot(ElementName = "SubAccounts")]
	public class SubAccounts
	{
		[XmlElement(ElementName = "SubAccount")]
		public List<SubAccount> SubAccount { get; set; }
	}

	[XmlRoot(ElementName = "PaymentItems")]
	public class PaymentItems
	{
		[XmlElement(ElementName = "Item")]
		public Item Item { get; set; }
		[XmlElement(ElementName = "SubAccounts")]
		public SubAccounts SubAccounts { get; set; }
	}

	[XmlRoot(ElementName = "SubAccount")]
	public class SubAccount
	{
		[XmlElement(ElementName = "AccountID")]
		public string AccountID { get; set; }
		[XmlElement(ElementName = "Amount")]
		public string Amount { get; set; }
	}

	[XmlRoot(ElementName = "Item")]
	public class Item
	{
		[XmlElement(ElementName = "ProductName")]
		public string ProductName { get; set; }
		[XmlElement(ElementName = "ProductCode")]
		public string ProductCode { get; set; }
		[XmlElement(ElementName = "Quantity")]
		public string Quantity { get; set; }
		[XmlElement(ElementName = "Price")]
		public string Price { get; set; }
		[XmlElement(ElementName = "Subtotal")]
		public string Subtotal { get; set; }
		[XmlElement(ElementName = "Tax")]
		public string Tax { get; set; }
		[XmlElement(ElementName = "Total")]
		public string Total { get; set; }
	}

    public class PaymentNotificationResponse
    {
        public Payments2 Payments { get; set; }
        public string RedirectUrl { get; set; }
        public string Hash { get; set; }
    }

    public class Payments2
    {
        public Payment2 Payment { get; set; }
        public string StatusMessage { get; set; }
    }

    public class Payment2
    {
		public string MerchantReference { get; set; }
		public string CustReference { get; set; }
		public string PaymentLogId { get; set; }
		public string Amount { get; set; }
		public string Status { get; set; }
		public string AlternateCustReference { get; set; }
		public string PaymentCurrency { get; set; }
		public string PaymentCurrencyCode { get; set; }
	}
}
