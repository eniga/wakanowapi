﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;
using WakanowApi.Models;
using WakanowApi.Utilities;

namespace WakanowApi.Services
{
    public class PaymentNotificationService
    {
        IConfiguration config;
        ILogger<PaymentNotificationService> logger;
        string BaseUrl = string.Empty;
        string hashkey = string.Empty;
        AuthService service;

        public PaymentNotificationService(IConfiguration configuration, ILogger<PaymentNotificationService> log)
        {
            config = configuration;
            logger = log;
            hashkey = configuration.GetValue<string>("Wakanow:HASHKEY");
            BaseUrl = configuration.GetValue<String>("Wakanow:BaseUrl");
            service = new AuthService(configuration, log);
        }

        public PaymentNotificationResponse GetPaymentNotification(PaymentNotificationRequestPayload request)
        {
            PaymentNotificationResponse response = new PaymentNotificationResponse();
            string log = string.Empty;
            try
            {
                string ApiUrl = "SVC/Pay.ashx";
                //string Hash = SHAUtility.GenerateSHA512String(
                //    request.payments.payment.PaymentLogId + request.payments.payment.CustReference + request.payments.payment.AlternateCustReference +
                //    request.payments.payment.Amount + request.payments.payment.PaymentItems.ItemCode + request.payments.payment.PaymentItems.ItemAmount
                //    + request.payments.payment.PaymentStatus + request.payments.payment.PaymentCurrency + request.payments.payment.PaymentCurrencyCode + hashkey
                //    );
                string hash = service.GenerateHash2(request.Payments.Payment);
                var payload = new PaymentNotificationRequest()
                {
                    Hash = hash,
                    Payments = request.Payments
                };
                log = Helper.ObjectToXml(payload);
                logger.LogInformation("Wakanow:: Payment Notification request:", log);
                RestClient client = new RestClient(BaseUrl);
                var req = new RestRequest(ApiUrl, method: Method.POST);
                req.AddXmlBody(payload);
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = Helper.XmlDeserialize<PaymentNotificationResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
            log = Helper.ObjectToJson(response);
            logger.LogInformation("Wakanow:: Payment Notification response:", log);
            return response;
        }
    }
}
