﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using WakanowApi.Models;
using WakanowApi.Utilities;

namespace WakanowApi.Services
{
    public class CustomerInformationService
    {
        IConfiguration config;
        ILogger<CustomerInformationService> logger;
        string BaseUrl = string.Empty;
        string hashkey = string.Empty;

        public CustomerInformationService(IConfiguration configuration, ILogger<CustomerInformationService> log)
        {
            config = configuration;
            logger = log;
            hashkey = configuration.GetValue<string>("Wakanow:HASHKEY");
            BaseUrl = configuration.GetValue<String>("Wakanow:BaseUrl");
        }

        public CustomerInformationResponse GetCustomerInformation(CustomerInformationRequestPayload request)
        {
            CustomerInformationResponse response = new CustomerInformationResponse();
            try
            {
                string ApiUrl = "SVC/Pay.ashx";
                string Hash = SHAUtility.GenerateSHA512String(
                    request.CustReference + request.MerchantReference + request.PaymentItemCode + hashkey
                    );
                var payload = new CustomerInformationRequest()
                {
                    Hash = Hash,
                    CustReference = request.CustReference,
                    MerchantReference = request.MerchantReference,
                    PaymentItemCode = request.PaymentItemCode
                };
                RestClient client = new RestClient(BaseUrl);
                var req = new RestRequest(ApiUrl, method: Method.POST);
                req.AddXmlBody(payload);
                var result = client.Execute(req);
                if (result.IsSuccessful)
                {
                    response = Helper.XmlDeserialize<CustomerInformationResponse>(result.Content);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
            return response;
        }
    }
}
