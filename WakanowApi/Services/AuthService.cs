﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WakanowApi.Models;
using WakanowApi.Utilities;

namespace WakanowApi.Services
{
    public class AuthService
    {
        IConfiguration config;
        ILogger<Object> logger;

        public AuthService(IConfiguration configuration, ILogger<Object> log)
        {
            config = configuration;
            logger = log;
        }

        public string GenerateHash(HashRequestPayload request)
        {
            string result = string.Empty;
            try
            {
                string hashkey = config.GetValue<string>("Wakanow:HASHKEY");
                string value = request.PaymentItemCode + request.MerchantReference + request.CustReference + hashkey;
                result = SHAUtility.GenerateSHA512String(value);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
            return result;
        }

        public string GenerateHash2(Payment request)
        {
            string result = string.Empty;
            try
            {
                string hashkey = config.GetValue<string>("Wakanow:HASHKEY");
                string value = request.PaymentLogId + request.CustReference + request.AlternateCustReference +
                    request.Amount + request.PaymentItems.PaymentItem.ItemCode + request.PaymentItems.PaymentItem.ItemAmount
                    + request.PaymentStatus + request.PaymentCurrency + request.PaymentCurrencyCode + hashkey;
                result = SHAUtility.GenerateSHA512String(value);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
            return result;
        }
    }
}
