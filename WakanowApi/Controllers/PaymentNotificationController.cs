﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WakanowApi.Models;
using WakanowApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WakanowApi.Controllers
{
    [Route("api/[controller]")]
    public class PaymentNotificationController : Controller
    {
        PaymentNotificationService service;

        public PaymentNotificationController(IConfiguration configuration, ILogger<PaymentNotificationService> log)
        {
            service = new PaymentNotificationService(configuration, log);
        }

        [HttpPost]
        public PaymentNotificationResponse GetPaymentNotification([FromBody] PaymentNotificationRequestPayload request)
        {
            var result = service.GetPaymentNotification(request);
            return result;
        }
    }
}
