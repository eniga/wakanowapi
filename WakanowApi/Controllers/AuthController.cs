﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WakanowApi.Models;
using WakanowApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WakanowApi.Controllers
{
    [Route("api/[controller]")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class AuthController : Controller
    {
        AuthService service;

        public AuthController(IConfiguration configuration, ILogger<AuthService> log)
        {
            service = new AuthService(configuration, log);
        }

        [HttpPost("hash")]
        public string GetHash([FromBody] HashRequestPayload request)
        {
            string result = service.GenerateHash(request);
            return result;
        }

        [HttpPost("hash2")]
        public string GenerateHash2([FromBody]Payment request)
        {
            string result = service.GenerateHash2(request);
            return result;
        }
    }
}
