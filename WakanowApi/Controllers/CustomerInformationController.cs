﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WakanowApi.Models;
using WakanowApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WakanowApi.Controllers
{
    [Route("api/[controller]")]
    public class CustomerInformationController : Controller
    {
        CustomerInformationService service;

        public CustomerInformationController(IConfiguration configuration, ILogger<CustomerInformationService> log)
        {
            service = new CustomerInformationService(configuration, log);
        }

        [HttpPost]
        public CustomerInformationResponse GetCustomerInformation([FromBody] CustomerInformationRequestPayload request)
        {
            var result = service.GetCustomerInformation(request);
            return result;
        }
    }
}
